defmodule AssertURL.Mixfile do
  use Mix.Project

  def project do
    [
      app: :assert_url,
      version: "0.1.0",
      elixir: "~> 1.3",
      description: "Collection of functions to test URLs",
      deps: deps,
      package: package
    ]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: []]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev}
    ]
  end

  defp package do
    [
      maintainers: ["Lucas Tolchinsky"],
      licenses: ["MIT"],
      links: %{"Gitlab" => "https://gitlab.com/tonchis/assert_url"},
      files: ~w{mix.exs README.md lib test}
    ]
  end
end
