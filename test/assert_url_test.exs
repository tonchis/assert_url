defmodule AssertURLTest do
  use ExUnit.Case
  doctest AssertURL

  setup do
    {:ok, [url: "http://sub.example.org:8080/path/to/resource?foo=bar&very=wow#fragment"]}
  end

  test "scheme_equal", %{url: url} do
    assert AssertURL.scheme_equal "http", url

    assert_raise AssertURL.SchemeError, fn ->
      AssertURL.scheme_equal "ftp", url
    end
  end

  test "host_equal", %{url: url} do
    assert AssertURL.host_equal "sub.example.org", url

    assert_raise AssertURL.HostError, fn ->
      AssertURL.host_equal "example.org", url
    end
  end

  test "port_equal", %{url: url} do
    assert AssertURL.port_equal 8080, url
    assert AssertURL.port_equal 80, "http://foo.com"
    assert AssertURL.port_equal 443, "https://foo.com"

    assert_raise AssertURL.PortError, fn ->
      AssertURL.port_equal 443, url
    end
  end

  test "path_equal", %{url: url} do
    assert AssertURL.path_equal "/path/to/resource", url
    assert AssertURL.path_equal "/", "http://foo.com/"
    assert AssertURL.path_equal nil, "http://foo.com"

    assert_raise AssertURL.PathError, fn ->
      AssertURL.path_equal "/foo", url
    end
  end

  test "query_equal", %{url: url} do
    assert AssertURL.query_equal "foo=bar&very=wow", url

    # Order matters.
    assert_raise AssertURL.QueryError, fn ->
      AssertURL.query_equal "very=wow&foo=bar", url
    end

    assert_raise AssertURL.QueryError, fn ->
      AssertURL.query_equal "sorry=missing", url
    end
  end

  test "query_include", %{url: url} do
    assert AssertURL.query_include [foo: "bar"], url
    assert AssertURL.query_include [very: "wow"], url
    assert AssertURL.query_include [foo: "bar", very: "wow"], url
    assert AssertURL.query_include [very: "wow", foo: "bar"], url
    assert AssertURL.query_include [], url

    assert_raise AssertURL.QueryKeyMissingError, fn ->
      AssertURL.query_include [sorry: "missing"], url
    end

    assert_raise AssertURL.QueryValueError, fn ->
      AssertURL.query_include [foo: "not"], url
    end
  end

  test "fragment_equal", %{url: url} do
    assert AssertURL.fragment_equal "fragment", url

    assert_raise AssertURL.FragmentError, fn ->
      AssertURL.fragment_equal "not", url
    end
  end
end
