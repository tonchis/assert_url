# AssertURL

This is my Elixir version of Ruby's [AssertURL](https://gitlab.com/tonchis/assert-url).

It's a collection of functions to test URLs. The main goal of this module is to develop a better
testing habit when dealing with URLs.

Instead of doing `assert url == "http://example.com/path?foo=bar"` where any part of the string will
make the expectation fail, `AssertURL` proposes to do the following:

```elixir
assert AssertURL.path_equal "/path", "http://example.com/path"
# => true

assert AssertURL.path_equal "/path", "http://example.com/poth"
# ** (AssertURL.PathError) Expected /path, got /poth.
# stacktrace:
#   ...
```

The error messages are colorized for readability.

The functions available are the following:

* `AssertURL.scheme_equal "http", "http://example.com"`
* `AssertURL.host_equal "example.com", "http://example.com"`
* `AssertURL.port_equal 80, "http://example.com"`
* `AssertURL.path_equal "/path", "http://example.com/path"`
* `AssertURL.query_include [foo: "bar"], "http://example.com/path?foo=bar&baz=wow"`
* `AssertURL.query_equal "foo=bar&baz=wow", "http://example.com/path?foo=bar&baz=wow"`
* `AssertURL.fragment_equal "frag", "http://example.com/path#frag"`

I recommend that you take a look at the tests for a full list of examples.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `assert_url` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:assert_url, "~> 0.1.0"}]
    end
    ```

  2. Ensure `assert_url` is started before your application:

    ```elixir
    def application do
      [applications: [:assert_url]]
    end
    ```

